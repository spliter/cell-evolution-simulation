/*
 * Copyright (c) 2015, Mikolaj Kuta
 * Licensed under BSD 3-Clause License
 * All rights reserved.
 */
 
/*
 * renderer.h
 *
 *  Created on: 26 gru 2014
 *      Author: Miko Kuta
 */

#ifndef CBB_GAME_RENDERER_H
#define CBB_GAME_RENDERER_H
#include "types.h"
#include "../math/vec2f.h"
#include "../math/vec3f.h"
#include "../math/vec4f.h"
#include "../math/matrix4x4f.h"
#include "../math/quaternionf.h"
#include "resources.h"
struct memory_manager;
//forward define

typedef int32 image_handle;

struct image_info
{
	uint32 width;
	uint32 height;
};

struct renderer_mesh_state
{
	int32 totalVertexCount;
	int32 usedVertexCount;
	int32 image;
	bool32 isInterlaced;
	int32 posType;
	int32 colorType;
	int32 texCoordType;
	bool32 hasNormals;
	vec4f color; //if the mesh doesn't use per vertex colors it'll use this one
	union
	{
		struct
		{
			union
			{
				real32 *pos;
				vec2f *pos2f;
				vec3f *pos3f;
			};
			union
			{
				real32 *colors;
				vec3f *colorsRGB;
				vec4f *colorsRGBA;
			};
			union
			{
				real32 *texCoords;
				vec2f *texCoords2f;
			};
			union
			{
				real32 *norm;
				vec3f *norm3f;
			};
		};
		real32* vertices;
	};
};

enum renderer_position_type
{
	RendererPosition2D = 2,
	RendererPosition3D = 3,
};

enum renderer_color_type
{
	RendererColorNone = 0,
	RendererColorRGB = 3,
	RendererColorRGBA = 4
};

enum renderer_tex_coord_type
{
	RendererTexCoordsNone = 0,
	RendererTexCoords2D = 2
};

class game_renderer
{
public:

	virtual void setView2D(real32 x, real32 y, real32 width, real32 height, real32 centerRatioX, real32 centerRatioY)=0;
	virtual void setView3D(real32 viewportX, real32 viewportY, real32 viewportWidth, real32 viewportHeight, real32 fov, real32 nearClip, real32 farClip)=0;

	virtual void clearView(real32 r, real32 g, real32 b)=0;

	virtual void setTransform(real32 x, real32 y, real32 scalex, real32 scaley, real32 rotateDeg)=0;
	virtual void mulTransform(real32 x, real32 y, real32 scalex, real32 scaley, real32 rotateDeg)=0;

	virtual void setTransform3D(real32 x, real32 y, real32 z, real32 scalex, real32 scaley, real32 scalez, quaternionf rot = _quaternionf(0, 0, 0, 1))=0;
	virtual void mulTransform3D(real32 x, real32 y, real32 z, real32 scalex, real32 scaley, real32 scalez, quaternionf rot = _quaternionf(0, 0, 0, 1))=0;

	virtual void setMatrix(matrix4x4f* mat)=0;
	virtual void mulMatrix(matrix4x4f* mat)=0;

	virtual void pushMatrix()=0;
	virtual void popMatrix()=0;

	virtual void drawRectangle(real32 x1, real32 y1, real32 x2, real32 y2, bool32 outline, real32 r, real32 g, real32 b, real32 a=1.0f)=0;
	virtual void drawCircle(real32 x, real32 y, real32 radius, int32 divisions, bool32 outline, real32 r, real32 g, real32 b, real32 a=1.0f)=0;
	virtual void drawLine(real32 x1, real32 y1, real32 x2, real32 y2, real32 r, real32 g, real32 b, real32 a=1.0f)=0;
	virtual void drawImage(real32 x1, real32 y1, real32 x2, real32 y2, image_handle image, real32 r, real32 g, real32 b, real32 a=1.0f)=0;

	virtual void drawBox(real32 x1, real32 y1, real32 z1, real32 x2, real32 y2, real32 z2, bool32 outline, real32 r, real32 g, real32 b, real32 a=1.0f)=0;
	virtual void drawSphere(real32 x, real32 y, real32 z, real32 radius, int32 divisions, bool32 outline, real32 r, real32 g, real32 b, real32 a=1.0f)=0;
	virtual void drawLine(real32 x1, real32 y1, real32 z1, real32 x2, real32 y2, real32 z2, real32 r, real32 g, real32 b, real32 a=1.0f)=0;

	virtual renderer_mesh_state allocTemporaryMesh(int32 vertexCount, image_handle image, bool32 isVertexDataInterlaced, renderer_position_type positionType, renderer_color_type colorType, renderer_tex_coord_type texCoordType, bool32 hasNormals=false)=0;
	virtual renderer_mesh_state allocStaticMesh(memory_manager *memoryManager, int32 vertexCount, image_handle image, bool32 isVertexDataInterlaced, renderer_position_type positionType, renderer_color_type colorType, renderer_tex_coord_type texCoordType, bool32 hasNormals=false)=0;
	virtual void drawMesh(renderer_mesh_state meshState)=0;

	virtual image_handle getImage(image_resource image)=0;
	virtual image_info getImageInfo(image_handle image)=0;

	virtual ~game_renderer()
	{
	}
	;
};

#endif
