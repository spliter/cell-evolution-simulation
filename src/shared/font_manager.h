/*
 * Copyright (c) 2015, Mikolaj Kuta
 * Licensed under BSD 3-Clause License
 * All rights reserved.
 */
 
/*
 * font_manager.h
 *
 *  Created on: 7 kwi 2015
 *      Author: Miko Kuta
 */

#ifndef FONT_MANAGER_H_
#define FONT_MANAGER_H_
#include "renderer.h"
#include "../math/box2f.h"
//TODO: allow to specify some stuff like line height/spacing, kerning
//TODO: allow for HTML-like text info for colors;
typedef int32 font_handle;
struct font_info
{
	font_handle handle;
	image_handle image;
};

struct font_render_mesh_state
{
	box2f boundingBox;
	renderer_mesh_state mesh;
};

struct font_manager
{
	virtual font_handle getFont(font_resource font)=0;
	virtual font_info getFontInfo(font_resource font)=0;
	virtual font_render_mesh_state createTemporaryText(const char* text, real32 x, real32 y, real32 size, font_handle font, real32 r, real32 g, real32 b, real32 a)=0;
	virtual font_render_mesh_state createStaticText(memory_manager* manager, const char* text, real32 x, real32 y, real32 size, font_handle font, real32 r, real32 g, real32 b, real32 a)=0;
	virtual ~font_manager(){};
};
#endif
