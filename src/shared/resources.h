/*
 * Copyright (c) 2015, Mikolaj Kuta
 * Licensed under BSD 3-Clause License
 * All rights reserved.
 */
 
/*
 * game_resources.h
 *
 *  Created on: 3 kwi 2015
 *      Author: Miko Kuta
 */

#ifndef GAME_RESOURCES_H_
#define GAME_RESOURCES_H_


//IMAGE
typedef signed int image_resource;

#define IMAGE_DEFS(name)\
		name(None,"")\
		name(Cell,"sprites/cell.png")\
		name(FoodPellet,"sprites/food_pellet.png")\
		name(TestFont,"fonts/test.png")\

#define IMAGE_ENUMS()\
		IMAGE_DEFS(IMAGE_ENUM_DEF)
#define IMAGE_ENUM_DEF(name, filename) name,


namespace Image
{
	enum
	{
		IMAGE_ENUMS()
		Count
	};
};



//FONT
typedef signed int font_resource;
#define FONT_DEFS(name)\
		name(None,"",Image::None)\
		name(TestFont,"fonts/test.fnt",Image::TestFont)

#define FONT_ENUMS()\
		FONT_DEFS(FONT_ENUM_DEF)
#define FONT_ENUM_DEF(name, filename, imageResource) name,

namespace Font
{
	enum
	{
		FONT_ENUMS()
		Count
	};
};

#endif
