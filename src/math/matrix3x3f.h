/*
 * Copyright (c) 2015, Mikolaj Kuta
 * Licensed under BSD 3-Clause License
 * All rights reserved.
 */
 
/*
 * matrix3x3f.h
 *
 *  Created on: 13-04-2011
 *      Author: Spliter
 */

#ifndef matrix3x3f_H_
#define matrix3x3f_H_
#include "mathUtils.h"
#include "vec2f.h"

/* Note: Column major matrices
 * pre-multiply for the desired result
 * ie: to rotate a translated matrix, you'd have to do:
 * Result=rotation*translation;
 */
struct matrix3x3f
{
public:
	union
	{
		real32 me[9];
		struct
		{
			real32 m00;
			real32 m10;
			real32 m20;

			real32 m01;
			real32 m11;
			real32 m21;

			real32 m02;
			real32 m12;
			real32 m22;
		};
	};

	void set(real32 val);
	void set(	real32 _a,real32 _b,real32 _c,
				real32 _d,real32 _e,real32 _f,
				real32 _g,real32 _h,real32 _i);
	void set(const matrix3x3f& mb);

	matrix3x3f& makeIdentity();
	matrix3x3f& makeRotation(real32 rad);
	matrix3x3f& makeRotationDeg(real32 deg);
	matrix3x3f& makeScale(real32 xscale,real32 yscale);
	matrix3x3f& makeTranslation(real32 xtranslate,real32 ytranslate);

	matrix3x3f& transpose();
	matrix3x3f transposed() const;

	matrix3x3f& inverse();
	matrix3x3f inversed() const;

	matrix3x3f& rotate(real32 rad);
	matrix3x3f rotated(real32 rad) const;
	matrix3x3f& rotateDeg(real32 deg);
	matrix3x3f rotatedDeg(real32 deg) const;

	matrix3x3f& scale(real32 _scale);
	matrix3x3f scaled(real32 _scale) const;
	matrix3x3f& scale(real32 xscale,real32 yscale);
	matrix3x3f scaled(real32 xscale,real32 _yscale) const;

	matrix3x3f& translate(real32 x,real32 y);
	matrix3x3f translated(real32 x,real32 y) const;


	matrix3x3f& transform(const matrix3x3f& b);
	matrix3x3f transformed(const matrix3x3f& b) const;

	matrix3x3f& operator=(const matrix3x3f& b);
	matrix3x3f& operator*=(const matrix3x3f& b);
	matrix3x3f& operator/=(const matrix3x3f& b);
	matrix3x3f& operator+=(const matrix3x3f& b);
	matrix3x3f& operator-=(const matrix3x3f& b);

	matrix3x3f operator*(const matrix3x3f& b) const;
	matrix3x3f operator/(const matrix3x3f& b) const;
	matrix3x3f operator+(const matrix3x3f& b) const;
	matrix3x3f operator-(const matrix3x3f& b) const;

	matrix3x3f& operator=(real32 val);
	matrix3x3f& operator*=(real32 val);
	matrix3x3f& operator/=(real32 val);

	matrix3x3f operator*(real32 val) const;
	matrix3x3f operator/(real32 val) const;

	vec2f operator*(const vec2f& v) const;
};
#endif /* matrix3x3f_H_ */
