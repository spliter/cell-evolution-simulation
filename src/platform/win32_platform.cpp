/*
 * Copyright (c) 2015, Mikolaj Kuta
 * Licensed under BSD 3-Clause License
 * All rights reserved.
 */

/*
 * win32platform.cpp
 *
 *  Created on: 21 gru 2014
 *      Author: Miko Kuta
 */

#include <windows.h>
#include <limits>
#include <Shlwapi.h>
#include <Xinput.h>
#include <gl/GL.h>
#include <gl/GLU.h>
#include <math.h>
#include <stdio.h>
#include <io.h>
#include <fcntl.h>
#include <vector>

#include "../shared/renderer.h"
#include "gl_renderer.cpp"
#include "bitmap_font_manager.cpp"
#include "../math/mathUtils.cpp"
#include "../math/matrix3x3f.cpp"
#include "../game/game.h"

global_variable const int defaultWidth = 960;
global_variable const int defaultHeight = 540;
#define MAX_FILENAME_LENGTH 1024

#define GAME_MEMORY_SIZE_MB 128
#define RENDERER_MEMORY_SIZE_MB 128
#define DEBUG_RENDERER_MEMORY_SIZE_MB 64
#define FONT_MEMORY_SIZE_MB 8 //small because we use mostly renderer memory
struct win32_game_code
{
	bool32 isLoaded;
	HMODULE module;
	uint64 lastWriteTime;
	game_update_and_render* gameUpdateAndRender;
};

struct win32_game_state
{
	uint64 gameMemorySize;
	uint8* gameMemoryBlock;

	uint64 rendererMemorySize;
	uint8* rendererMemoryBlock;

	uint64 debugRendererMemorySize;
	uint8* debugRendererMemoryBlock;

	uint64 fontMemorySize;
	uint8* fontMemoryBlock;

	bool32 isRunning;

	game_memory memory;
	game_screen screen;
	game_input input;
};

struct win32_platform_state
{
	HWND window;
	HCURSOR cursor;
	WNDCLASS windowClass;
};

global_variable win32_game_state gameState = { };
global_variable win32_platform_state platformState = { };

uint64 platformGetFileModifiedTime(const char* fileName)
{
	uint64 result = 0;
	WIN32_FILE_ATTRIBUTE_DATA fileAttributes;
	if (GetFileAttributesEx(fileName, GetFileExInfoStandard, &fileAttributes))
	{
		FILETIME lastWriteTime = fileAttributes.ftLastWriteTime;
		result = (uint64) (lastWriteTime.dwLowDateTime) | (uint64) (lastWriteTime.dwHighDateTime) << 32;
	}
	return result;
}

void setCursorPos(uint32 cursorWindowX, uint32 cursorWindowY)
{
	POINT mousePos = { cursorWindowX, cursorWindowY };
	ClientToScreen(platformState.window, &mousePos);
	SetCursorPos(mousePos.x, mousePos.y);
}

local void win32ConcatStrings(const char* str1, const char* str2, char* result, uint32 resultbufferLength)
{
	size_t length1 = strlen(str1);
	size_t length2 = strlen(str2);

	Assert((length1 + length2) < resultbufferLength);

//	memset((void*)result,'x',resultbufferLength);
	memcpy((void*) result, (const void*) str1, length1);
	memcpy((void*) (result + length1), (const void*) str2, length2);
	result[length1 + length2] = 0;
}

local win32_game_code win32LoadGameCode(const char* filename)
{
	printf("Loading game code from %s\n", filename);
	win32_game_code result = { };
	const char tempFileName[] = "game.dll.temp";

	bool32 fileExists = PathFileExists(filename);

	if (fileExists)
	{
		CopyFile(filename, tempFileName, false);

		result.module = LoadLibrary(tempFileName);
		if (result.module)
		{
			result.lastWriteTime = platformGetFileModifiedTime(filename);
			result.isLoaded = true;
			result.gameUpdateAndRender = (game_update_and_render*) GetProcAddress(result.module, STRINGIZE(GAME_UPDATE_AND_RENDER_NAME));
			if (!result.gameUpdateAndRender)
			{
				result.isLoaded = false;
				printf("Failed to load function %s\n", STRINGIZE(GAME_UPDATE_AND_RENDER_NAME));
			}
		}
		else
		{
			printf("Failed to load module %s\n", filename);
		}
	}
	else
	{
		DeleteFileA(tempFileName);
		printf("File doesn't exists: %s\n", filename);
	}
	return result;
}

local void win32UnloadGameCode(win32_game_code* code)
{
	if (code->module != 0)
	{
		FreeLibrary(code->module);
	}

	code->isLoaded = false;
	code->module = 0;
	code->gameUpdateAndRender = NULL;

}

local void win32ProcessButtonState(button_state *button, bool32 isDown)
{
	if (isDown != button->isDown)
	{
		button->switchCount++;
		button->isDown = isDown;
	}
}

local LRESULT CALLBACK win32ProcessWindowMessages(HWND window, UINT message, WPARAM wParam, LPARAM lParam)
{
	LRESULT result = 0;

	switch (message)
	{
		case WM_SIZE:
		{
			int width = (int16) lParam;
			int height = (int16) (lParam >> 16);
			gameState.screen.width = width;
			gameState.screen.height = height;
		}
		break;

		case WM_DESTROY:
		{

		}
		break;

		case WM_CLOSE:
		{
			gameState.isRunning = false;
			PostQuitMessage(0);
		}
		break;

		default:
		{
			result = DefWindowProc(window, message, wParam, lParam);
		}
	}
	return result;
}

#define KEY_IS_DOWN(lparam) ((lparam&0x40000000L)==0)
#define KEY_WAS_DOWN(lparam) ((lparam&0x80000000L)!=0)
#define PARAM_GET_HIGH_WORD(param) ((param>>16)&0xffff)
#define PARAM_GET_LOW_WORD(param) (param&0xffff)

local void win32ProcessPendingMessages(HWND window, win32_game_state* gameState)
{
	MSG message = { };

	while (PeekMessage(&message, 0, 0, 0, PM_REMOVE))
	{
		switch (message.message)
		{
			case WM_QUIT:
			gameState->isRunning = false;
			break;

			case WM_SYSKEYDOWN:
			case WM_SYSKEYUP:
			case WM_KEYDOWN:
			case WM_KEYUP:
			{
				uint32 vkCode = (uint32) message.wParam;

				bool32 wasDown = KEY_WAS_DOWN(message.lParam);
				bool32 isDown = KEY_IS_DOWN(message.lParam);
				0x10000000;

				if (wasDown != isDown)
				{
					switch (message.wParam)
					{
						case VK_ESCAPE:
						if (isDown)
						{
							gameState->isRunning = false;
						}
						break;

						case VK_ADD:
						{
							win32ProcessButtonState(&gameState->input.buttonSpeedUp, isDown);
						}
						break;
						case VK_SUBTRACT:
						{
							win32ProcessButtonState(&gameState->input.buttonSlowDown, isDown);
						}
						break;
						case VK_LEFT:
						{
							win32ProcessButtonState(&gameState->input.buttonSelectPrev, isDown);
						}
						break;
						case VK_DOWN:
						{
							win32ProcessButtonState(&gameState->input.buttonNextOp, isDown);
						}
						break;
						case VK_RIGHT:
						{
							win32ProcessButtonState(&gameState->input.buttonSelectNext, isDown);
						}
						break;
						case VK_RETURN:
						{
							win32ProcessButtonState(&gameState->input.buttonStopOnGeneration, isDown);
						}
						break;
						case VK_SPACE:
						{
							win32ProcessButtonState(&gameState->input.buttonToggleDebug, isDown);
						}
						break;
						case VK_F1:
						{
							win32ProcessButtonState(&gameState->input.buttonToggleHelp, isDown);
						}
						break;
						case VK_F2:
						{
							win32ProcessButtonState(&gameState->input.buttonToggleInfo, isDown);
						}
						break;
						case VK_F3:
						{
							win32ProcessButtonState(&gameState->input.buttonToggleGraphics, isDown);
						}
						break;
					}
				}
			}
			break;

			case WM_MOUSEWHEEL:
			{
				int32 delta = GET_WHEEL_DELTA_WPARAM(message.wParam);
				if (delta > 0) //zoom out
				{
					gameState->input.buttonZoomOut.isDown = false;
					gameState->input.buttonZoomOut.switchCount = 2;

				}
				else if (delta < 0) //zoom in
				{
					gameState->input.buttonZoomIn.isDown = false;
					gameState->input.buttonZoomIn.switchCount = 2;
				}
			}
			break;

			case WM_LBUTTONUP:
			case WM_LBUTTONDOWN:
			{
				bool32 isDown = message.message == WM_LBUTTONDOWN;
				win32ProcessButtonState(&gameState->input.buttonMouseLeft, isDown);
			}
			break;

			case WM_RBUTTONUP:
			case WM_RBUTTONDOWN:
			{
				bool32 isDown = message.message == WM_RBUTTONDOWN;
				win32ProcessButtonState(&gameState->input.buttonMouseRight, isDown);
			}
			break;

			case WM_MBUTTONUP:
			case WM_MBUTTONDOWN:
			{
				bool32 isDown = message.message == WM_MBUTTONDOWN;
				win32ProcessButtonState(&gameState->input.buttonMouseMiddle, isDown);
			}
			break;

			default:
			{
				TranslateMessage(&message);
				DispatchMessage(&message);
			}
		}
	}
}

void printMemoryUsage()
{
	MEMORYSTATUSEX memoryStatus =
		{ };
	memoryStatus.dwLength = sizeof(memoryStatus);

	BOOL memoryStatusResult = GlobalMemoryStatusEx(&memoryStatus);
	printf("\nMEMORY SPECS BEFORE GAME MEMORY ALLOC\n");
	if (memoryStatusResult)
	{
		printf("Total Physical Memory: %llu kb(%llu mb)\n", (uint64) (ToKilobytes(memoryStatus.ullAvailPhys)), (uint64) (ToMegabytes(memoryStatus.ullTotalPhys)));
		printf("Free Physical Memory: %llu kb(%llu mb)\n", (uint64) (ToKilobytes(memoryStatus.ullAvailPhys)), (uint64) (ToMegabytes(memoryStatus.ullAvailPhys)));
		printf("Total Virtual Memory: %llu kb(%llu mb)\n", (uint64) (ToKilobytes(memoryStatus.ullTotalVirtual)), (uint64) (ToMegabytes(memoryStatus.ullTotalVirtual)));
		printf("Free Virtual Memory: %llu kb(%llu mb)\n", (uint64) (ToKilobytes(memoryStatus.ullAvailVirtual)), (uint64) (ToMegabytes(memoryStatus.ullAvailVirtual)));
	}
	else
	{
		printf("Retrieving memory info failed, error: %d\n", GetLastError());
	}
}

void showMemoryErrorBox(const char* name, void* address, uint64 size, DWORD error)
{
	char buffer[512];
	char bufferTitle[128];
	sprintf_s(buffer, sizeof(buffer),
		"Couldn't allocate memory for %s at address 0x%08x with size %llu kb(%llu mb)",
		name,
		address,
		(uint64) (ToKilobytes(size)),
		(uint64) (ToMegabytes(size)));
	sprintf_s(bufferTitle, sizeof(bufferTitle), "ERROR: %d", error);

	MessageBox(NULL, buffer, bufferTitle, MB_OK | MB_ICONEXCLAMATION);
}

bool32 allocMemoryForSystem(const char* systemName, uint8* baseAddress, uint64 size, uint8** retBlockPtr, uint64* retSizePtr)
{
	uint8* result = (uint8*) VirtualAlloc(baseAddress, (SIZE_T) size, MEM_RESERVE | MEM_COMMIT, PAGE_READWRITE);
	if (result == NULL)
	{
		showMemoryErrorBox(systemName, baseAddress, size, GetLastError());
		return false;
	}
	else
	{
		*retBlockPtr = result;
		if (retSizePtr != NULL)
		{
			*retSizePtr = size;
		}
		printf("%s memory address = 0x%08x\n", systemName, (uint32) result);
		return true;
	}
}

real32 normalizeThumbstick(SHORT xThumb)
{
	int thumbDeadZone = 2000;
	float thumbScale = 2;

	real32 result = 0;
	if (xThumb < -thumbDeadZone)
	{
		result = -normalize((real32) thumbDeadZone, (real32) 65000, (real32) -xThumb);
		result = clamp(-1, 1, result * thumbScale);
	}
	else if (xThumb > thumbDeadZone)
	{
		result = normalize((real32) thumbDeadZone, (real32) 65000, (real32) xThumb);
		result = clamp(-1, 1, result * thumbScale);
	}
	return result;
}

void createConsoleWindow()
{
	AllocConsole();
	int outConsole;
	int inConsole;
	HANDLE stdOutHandle;
	HANDLE stdInHandle;
	FILE *outFile;
	FILE *inFile;
	stdOutHandle = GetStdHandle(STD_OUTPUT_HANDLE);
	stdInHandle = GetStdHandle(STD_INPUT_HANDLE);
	outConsole = _open_osfhandle((long) stdOutHandle, _O_TEXT);
	inConsole = _open_osfhandle((long) stdInHandle, _O_TEXT);

	outFile = _fdopen(outConsole, "w");
	*stdout = *outFile;
	setvbuf(stdout, NULL, _IONBF, 0);

	inFile = _fdopen(inConsole, "r");
	*stdin = *inFile;
	setvbuf(stdout, NULL, _IONBF, 0);

	HWND consoleWindow = GetConsoleWindow();
	SetWindowPos(consoleWindow, NULL, 0, 0, 0, 0, SWP_NOSIZE);
}

int CALLBACK WinMain(HINSTANCE AppInstance, HINSTANCE PrevAppInstance, LPSTR commandLine, int showCommand)
{
	createConsoleWindow();

	platformState.cursor = LoadCursor(NULL, IDC_ARROW);

	platformState.windowClass.style = CS_CLASSDC | CS_HREDRAW | CS_VREDRAW;
	platformState.windowClass.lpfnWndProc = win32ProcessWindowMessages;
	platformState.windowClass.hInstance = AppInstance;
	platformState.windowClass.hCursor = platformState.cursor;
	platformState.windowClass.lpszClassName = "LitterboxWin32Window";

	if (!RegisterClass(&platformState.windowClass))
	{
		MessageBox(NULL, "Can't register window class", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return 1;
	}

	DWORD windowStyle = WS_OVERLAPPEDWINDOW | WS_VISIBLE;

	RECT windowRect =
		{ };
	windowRect.left = 0;
	windowRect.top = 0;
	windowRect.right = defaultWidth;
	windowRect.bottom = defaultHeight;
	AdjustWindowRect(&windowRect, windowStyle, false);

	HWND desktop = GetDesktopWindow();
	WINDOWINFO desktopInfo;
	GetWindowInfo(desktop, &desktopInfo);
	int32 desktopWidth = desktopInfo.rcClient.right - desktopInfo.rcClient.left;
	int32 desktopHeight = desktopInfo.rcClient.bottom - desktopInfo.rcClient.top;

	int32 windowWidth = windowRect.right - windowRect.left;
	int32 windowHeight = windowRect.bottom - windowRect.top;
	int32 windowPosX = (desktopWidth - windowWidth) / 2;
	int32 windowPosY = (desktopHeight - windowHeight) / 2;

	platformState.window = CreateWindowEx(0, platformState.windowClass.lpszClassName, "Litterbox Experimental Engine", windowStyle, windowPosX, windowPosY, windowWidth, windowHeight, 0, 0, AppInstance, 0);
	if (!platformState.window)
	{
		MessageBox(NULL, "Couldn't create window", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return 1;
	}

	PIXELFORMATDESCRIPTOR pixelFormatDescriptor =
		{ };

	pixelFormatDescriptor.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pixelFormatDescriptor.nVersion = 1;
	pixelFormatDescriptor.dwFlags = PFD_DRAW_TO_WINDOW |	//Must support drawing to window
		PFD_SUPPORT_OPENGL |	//Must support openGL
		PFD_DOUBLEBUFFER;	//Must support double buffering
	pixelFormatDescriptor.iPixelType = PFD_TYPE_RGBA;
	pixelFormatDescriptor.cColorBits = 32;
	pixelFormatDescriptor.cDepthBits = 32;
	pixelFormatDescriptor.iLayerType = PFD_MAIN_PLANE;

	HDC deviceContext = GetDC(platformState.window);

	if (!deviceContext)
	{
		MessageBox(NULL, "Couldn't obtain Device context", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return 1;
	}

	int pixelFormat = ChoosePixelFormat(deviceContext, &pixelFormatDescriptor);
	if (!pixelFormat)
	{
		MessageBox(NULL, "Couldn't choose Pixel Format", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return 1;
	}

	if (!SetPixelFormat(deviceContext, pixelFormat, &pixelFormatDescriptor))
	{
		MessageBox(NULL, "Couldn't set Pixel Format", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return 1;
	}

	HGLRC glDeviceContext = wglCreateContext(deviceContext);

	if (!glDeviceContext)
	{
		MessageBox(NULL, "Couldn't create OpenGl Device context", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return 1;
	}

	if (!wglMakeCurrent(deviceContext, glDeviceContext))
	{
		MessageBox(NULL, "Couldn't make gl context current", "ERROR", MB_OK | MB_ICONEXCLAMATION);
		return 1;
	}

	ShowWindow(platformState.window, SW_SHOW);
	SetForegroundWindow(platformState.window);
	SetFocus(platformState.window);

	LARGE_INTEGER timerFrequency =
		{ };
	QueryPerformanceFrequency(&timerFrequency);

	gameState.isRunning = true;

	printf("\nMEMORY SPECS BEFORE GAME MEMORY ALLOC\n");
	printMemoryUsage();

	if (!allocMemoryForSystem("game",
	NULL,
		Megabytes(GAME_MEMORY_SIZE_MB),
		&gameState.gameMemoryBlock,
		&gameState.gameMemorySize
		))
	{
		return 1;
	}
	printf("\nMEMORY SPECS AFTER GAME MEMORY ALLOC\n");
	printMemoryUsage();

	if (!allocMemoryForSystem("renderer",
	NULL,
		Megabytes(RENDERER_MEMORY_SIZE_MB),
		&gameState.rendererMemoryBlock,
		&gameState.rendererMemorySize
		))
	{
		return 1;
	}

	if (!allocMemoryForSystem("debug renderer",
	NULL,
		Megabytes(DEBUG_RENDERER_MEMORY_SIZE_MB),
		&gameState.debugRendererMemoryBlock,
		&gameState.debugRendererMemorySize
		))
	{
		return 1;
	}

	if (!allocMemoryForSystem("font",
	NULL,
		Megabytes(FONT_MEMORY_SIZE_MB),
		&gameState.fontMemoryBlock,
		&gameState.fontMemorySize
		))
	{
		return 1;
	}

	printf("\nMEMORY SPECS AFTER LOOP MEMORY ALLOC\n");
	printMemoryUsage();

	gameState.memory.gameMemorySize = gameState.gameMemorySize;
	gameState.memory.gameMemory = gameState.gameMemoryBlock;
	gameState.memory.platformSetCursorPos = setCursorPos;

	gl_game_renderer* renderer = gl_game_renderer::create(gameState.rendererMemoryBlock, gameState.rendererMemorySize);

	gl_game_renderer* debugRenderer = gl_game_renderer::create(gameState.debugRendererMemoryBlock, gameState.debugRendererMemorySize);

	bitmap_font_manager* fontManager = bitmap_font_manager::create(renderer, gameState.fontMemoryBlock, gameState.fontMemorySize);

	gameState.screen.width = defaultWidth;
	gameState.screen.height = defaultHeight;
	gameState.memory.renderer = renderer;
	gameState.memory.debugRenderer = debugRenderer;
	gameState.memory.fontManager = fontManager;

	//Engine init

	win32_game_code gameCode;
	char* gameCodeFilename = "game.dll";
	char gameCodeFilenameBuffer[MAX_FILENAME_LENGTH];
	bool32 gameCodeFileExists = PathFileExists(gameCodeFilename);
	if (!gameCodeFileExists)
	{
		char gameExecutablePath[MAX_FILENAME_LENGTH + 1];
		gameExecutablePath[MAX_FILENAME_LENGTH] = 0;
		DWORD filenameResult = 0;
		filenameResult = GetModuleFileNameA(NULL, gameExecutablePath, MAX_FILENAME_LENGTH);
		size_t moduleLen = strlen(gameExecutablePath);
		int32 directoryLen = -1;
		for (size_t curChar = moduleLen - 1; curChar >= 0; curChar--)
		{
			if (gameExecutablePath[curChar] == '\\')
			{
				directoryLen = curChar;
				break;
			}
		}
		gameExecutablePath[directoryLen + 1] = 0;
		win32ConcatStrings(gameExecutablePath, gameCodeFilename, gameCodeFilenameBuffer, MAX_FILENAME_LENGTH);

		gameCodeFilename = gameCodeFilenameBuffer;
	}

	gameCode = win32LoadGameCode(gameCodeFilename);

	POINT mousePos;

	LARGE_INTEGER prevTime =
		{ };
	LARGE_INTEGER curTime =
		{ };
	LARGE_INTEGER counterFrequency;
	QueryPerformanceCounter(&prevTime);
	QueryPerformanceFrequency(&counterFrequency);
	printf("Performance Counter frequency: %llu\n", (uint64) counterFrequency.QuadPart);
	uint32 targetFps = 30;
	uint64 frameDuration = (1000 / targetFps);
	real32 frameTimeDif = 1.0f / targetFps;

	uint32 framesElapsed = 0;
	LARGE_INTEGER frameCountStart =
		{ };
	LARGE_INTEGER frameCountEnd =
		{ };
	real64 frameDurationTotal = 0;

	while (gameState.isRunning)
	{
		uint64 curGameCodeWriteTime = platformGetFileModifiedTime(gameCodeFilename);
		if (curGameCodeWriteTime != gameCode.lastWriteTime)
		{
			win32UnloadGameCode(&gameCode);
			gameCode = win32LoadGameCode(gameCodeFilename);
		}
		renderer->checkIfResourcesChanged();

		QueryPerformanceCounter(&frameCountStart);
		for (int i = 0; i < giButtonCount; i++)
		{
			gameState.input.buttons[i].switchCount = 0;
		}

		win32ProcessPendingMessages(platformState.window, &gameState);

		GetCursorPos(&mousePos);
		ScreenToClient(platformState.window, &mousePos);

		gameState.input.mouseX = (real32) mousePos.x;
		gameState.input.mouseY = (real32) mousePos.y;

		renderer->resetFrame();
		debugRenderer->resetFrame();

		if (gameCode.isLoaded)
		{
			gameCode.gameUpdateAndRender(&gameState.memory, &gameState.screen, &gameState.input, frameTimeDif);
		}

		debugRenderer->setView2D(0, 0, (real32) gameState.screen.width, (real32) gameState.screen.height, 0, 0);

		QueryPerformanceCounter(&frameCountEnd);

		LONGLONG timeDif = frameCountEnd.QuadPart - frameCountStart.QuadPart;
		real64 timeDifR = timeDif * 1000.0 / (real64) counterFrequency.QuadPart;

		frameDurationTotal += timeDifR;
		framesElapsed++;
		if (framesElapsed == 100)
		{
			printf("Time per frame: %f\n", (real32) (frameDurationTotal / framesElapsed));
			framesElapsed = 0;
			frameDurationTotal = 0;
		}

		renderer->drawFrame();
		debugRenderer->drawFrame();
		SwapBuffers(deviceContext);

		real64 duration = 0;
		do
		{
			QueryPerformanceCounter(&curTime);
			duration = (curTime.QuadPart - prevTime.QuadPart) * 1000 / (double) counterFrequency.QuadPart;
		}
		while (duration < frameDuration);
		prevTime = curTime;
	}

	wglMakeCurrent(deviceContext, NULL);

	return 0;
}
