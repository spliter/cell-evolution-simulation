/*
 * Copyright (c) 2015, Mikolaj Kuta
 * Licensed under BSD 3-Clause License
 * All rights reserved.
 */
/*
 * game.cpp
 *
 *  Created on: 25 gru 2014
 *      Author: Miko Kuta
 */
#ifndef GAME_CPP
#define GAME_CPP
#include "game.h"
#include "camera.h"
#include "../shared/resources.h"
#include "../math/box2f.h"
#include "../math/mathUtils.h"
#include "../math/mathUtils.cpp"
#include "../math/random.h"
#include "../math/vec2f.h"

#include <math.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>

#define MAX_CODE_LENGTH 4096
#define MEMORY_SIZE 256
#define MAX_REGISTER_COUNT 16
#define MAX_CELLS 128
#define MAP_WIDTH 64
#define MAP_HEIGHT 32
#define MAP_CENTER_X 32
#define MAP_CENTER_Y 16
#define MAP_CELL_SIZE 16

int32 resourcePerStepConsumption = 1;
int32 pelletResourceGain = 30;
int32 cellStartingResources = 220;
uint32 pelletChance = 5000;
uint32 startPelletChance = 8;
uint32 generationMaxSimulationSteps = 10200;
uint32 generationMinLivingCells = 1;
const uint32 generationInheritedCells = 24;
int32 generationUnmodifiedGenomes = 16;
int32 opCountPerStep = 16;
uint32 copyErrorChance = 32;

//TODO: figure out a way to add noOp to this list and still have it =0 in enum
#define OP_DEF(name,val) name(val,STRINGIZE(val))
#define OP_DEFS(name)\
	OP_DEF(name,opMoveBody)   /* left=0,right,up,down, only first two bits count U8[0]*/\
	OP_DEF(name,opMoveBodyM)  /*srcAddress U16[1], same as above instead it uses value from an address instead of the input value*/\
	/*arithmetic instructions*/\
	OP_DEF(name,opAdd)        /*dstAddress U16[0], srcAddress U16[1], srcValue S32[1]*/\
	OP_DEF(name,opSubtract)   /*dstAddress U16[0], srcAddress U16[1], srcValue S32[1]*/\
	OP_DEF(name,opMultiply)   /*dstAddress U16[0], srcAddress U16[1], srcValue S32[1]*/\
	OP_DEF(name,opDivide)     /*dstAddress U16[0], srcAddress U16[1], srcValue S32[1]*/\
	OP_DEF(name,opBitAnd)     /*dstAddress U16[0], srcAddress U16[1], srcValue S32[1]*/\
	OP_DEF(name,opBitOr)      /*dstAddress U16[0], srcAddress U16[1], srcValue S32[1]*/\
	OP_DEF(name,opBitNot)     /*dstAddress U16[0], srcAddress U16[1], srcValue S32[1]*/\
	/*memory arithmetic instructions*/\
	OP_DEF(name,opAddM)       /*dstAddress U16[0], srcAddressLhs U16[1], srcAddressRhs U16[2]*/\
	OP_DEF(name,opSubtractM)  /*dstAddress U16[0], srcAddressLhs U16[1], srcAddressRhs U16[2]*/\
	OP_DEF(name,opMultiplyM)  /*dstAddress U16[0], srcAddressLhs U16[1], srcAddressRhs U16[2]*/\
	OP_DEF(name,opDivideM)    /*dstAddress U16[0], srcAddressLhs U16[1], srcAddressRhs U16[2]*/\
	OP_DEF(name,opBitAndM)    /*dstAddress U16[0], srcAddressLhs U16[1], srcAddressRhs U16[2]*/\
	OP_DEF(name,opBitOrM)     /*dstAddress U16[0], srcAddressLhs U16[1], srcAddressRhs U16[2]*/\
	/*Memory ops*/\
	OP_DEF(name,opCopy)                   /*dstAddress U16[0], srcAddress U16[1])*/\
    OP_DEF(name,opSet)                    /*dstAddress U16[0], srcValue   S32[1])*/\
    /*jump instructions*/\
    OP_DEF(name,opJumpIfEqualZero)        /*srcAddress U16[0], opIndex U32[1]*/\
    OP_DEF(name,opJumpIfLessThanZero)     /*srcAddress U16[0], opIndex U32[1]*/\
    OP_DEF(name,opJumpIfLessOrEqualZero)  /*srcAddress U16[0], opIndex U32[1]*/\
    OP_DEF(name,opJumpIfMoreThanZero)     /*srcAddress U16[0], opIndex U32[1]*/\
    OP_DEF(name,opJumpIfMoreOrEqual)      /*srcAddress U16[0], opIndex U32[1]*/\
    OP_DEF(name,opJump)                   /*not used		 , opIndex U32[1]*/\
    /*Other*/\
    OP_DEF(name,opWait)                   /**/

#define OP_ENUMS()\
		OP_DEFS(OP_ENUM_DEF)
#define OP_ENUM_DEF(name, filename) name,

enum op_type
{
	opNoOp = 0,
	OP_ENUMS()
	opCount,
};

#define OP_STRINGS()\
		OP_DEFS(OP_STRING_DEF)
#define OP_STRING_DEF(name, stringName) stringName,

const char* op_names[] = {
		"opNoOp",
		OP_STRINGS()
};

struct instruction_state
{
	int32 op; // 32 but for alignment's sake
	union //64 bits for arguments
	{
		int8 arg8[8];
		int16 arg16[4];
		int32 arg32[2];

		uint8 uarg8[8];
		uint16 uarg16[4];
		uint32 uarg32[2];
	};
};

instruction_state moveBodyOp(uint8 direction)
{
	instruction_state result = { };
	result.op = opMoveBody;
	result.uarg8[0] = direction;
	return result;
}

instruction_state moveBodyOpM(uint16 srcAddress)
{
	instruction_state result = { };
	result.op = opMoveBodyM;
	result.uarg16[0] = srcAddress;
	return result;
}

instruction_state singleOp(op_type type)
{
	instruction_state result = { };
	result.op = type;
	return result;
}

instruction_state arithmeticOp(op_type type, uint16 dst, uint16 src, int32 value)
{
	instruction_state result = { };
	result.op = type;
	result.uarg16[0] = dst;
	result.uarg16[1] = src;
	result.uarg32[1] = value;
	return result;
}

instruction_state jumpOp(op_type type, uint16 src, int32 value)
{
	instruction_state result = { };
	result.op = type;
	result.uarg16[0] = src;
	result.uarg32[1] = value;
	return result;
}

#define cellAddressOf(varName) ((uint16)((uint32)(&(((cell_state*)0)->memory.varName))/4))

struct cell_state
{
	union
	{
		struct
		{
			//protected
			int32 resources; //if resources is 0 it dies
			int32 currentOp; //operation counter
			int32 posX;
			int32 posY;
			int32 quorum[5]; //quorum to left, right, top, bottom, and center in that order
			//free
			int32 registers[MAX_REGISTER_COUNT]; //just for convenience
			int32 data[1]; // hack to access rest of memory. not sure what else could be done
		};
		int32 contents[MEMORY_SIZE];
	} memory;
	bool32 isAlive;
	int32 movement;
	uint32 aliveSteps;
	bool32 consume;
	vec3f color;
	int32 getValue(uint32 address)
	{
		if (address >= 0 && address < MEMORY_SIZE)
		{
//			if (address >= cellAddressOf(registers[0]) && address <= cellAddressOf(registers[MAX_REGISTER_COUNT-1]))
//			{
//				printf("READING REGISTERS!\n");
//			}
//			else if (address >= cellAddressOf(quorum[0]) && address <= cellAddressOf(quorum[MAX_REGISTER_COUNT-1]))
//			{
//				printf("READING QUORUM!\n");
//			}
//			else if (address == cellAddressOf(posX) || address == cellAddressOf(posY))
//			{
//				printf("READING POSITION!\n");
//			}
			return memory.contents[address];
		}
		return 0;
	}
	void storeValue(uint32 address, int32 value)
	{
		if (address >= cellAddressOf(registers[0]) && address < MEMORY_SIZE)
		{
			memory.contents[address] = value;
		}
	}
};

struct cell_genome
{
	instruction_state code[MAX_CODE_LENGTH];
};

struct quorum_cell
{
	uint8 hasPellet;
	uint8 quorum;
};
struct game_state
{
	font_handle debugFont;
	memory_manager gameMemory;
	random_state random;
	vec2f prevMousePos;
	game_camera_2d worldCamera;
	game_camera_2d uiCamera;
	image_handle cellSprite;
	image_handle foodPelletSprite;
	int32 selectedCell;
	int32 frameCount;
	bool32 isInDebugMode;
	bool32 isHelpVisible;
	bool32 isInfoVisible;
	bool32 areGraphicsEnabled;
	bool32 showInfo;
	int32 debugExecutedCodeSteps;
	int32 stepsPerFrame;
	int32 genCount;
	int32 aliveCellCount;
	cell_state cell[MAX_CELLS];
	cell_genome cellCode[MAX_CELLS];
	quorum_cell quorumMap[MAP_WIDTH * MAP_HEIGHT];
};

void generateDefaultGenome(game_state *gameState, cell_genome* genome)
{
	/*
	 * Move right, wait, repeat
	 */

	uint32 moveRight = 40;
//	genome->code[0] = jumpOp(opJump, 0, moveRight);
//
//	genome->code[moveRight] = moveBodyOp(1);
//	genome->code[moveRight + 1] = singleOp(opWait);
//	genome->code[moveRight + 2] = jumpOp(opJump, 0, 0);

	//This is the desired algorithm, ideally the cells would evolve into something that resembles this hill climbing algorithm

//	uint32 testLeft = 50;
//	uint32 testRight = 60;
//	uint32 testTop = 70;
//	uint32 testBottom = 80;
//	uint32 testElse = 90;
//	uint32 testEnd = 100;
//
//	uint16 reg0 = cellAddressOf(registers[0]);
//	uint16 quorum0 = cellAddressOf(quorum[0]);
//	uint16 quorum1 = cellAddressOf(quorum[1]);
//	uint16 quorum2 = cellAddressOf(quorum[2]);
//	uint16 quorum3 = cellAddressOf(quorum[3]);
//	uint16 quorum4 = cellAddressOf(quorum[4]);
//
//	ZERO_STRUCT(cell_genome, genome);
//
//	genome->code[0] = jumpOp(opJump, 0, testLeft);
//
//	genome->code[testLeft] = arithmeticOp(opSubtractM, reg0, quorum4, quorum0);
//	genome->code[testLeft + 1] = jumpOp(opJumpIfMoreThanZero, reg0, testRight);
//	genome->code[testLeft + 2] = moveBodyOp(0);
//	genome->code[testLeft + 3] = jumpOp(opJump, 0, testEnd);
//
//	genome->code[testRight] = arithmeticOp(opSubtractM, reg0, quorum4, quorum1);
//	genome->code[testRight + 1] = jumpOp(opJumpIfMoreThanZero, reg0, testTop);
//	genome->code[testRight + 2] = moveBodyOp(1);
//	genome->code[testRight + 3] = jumpOp(opJump, 0, testEnd);
//
//	genome->code[testTop] = arithmeticOp(opSubtractM, reg0, quorum4, quorum2);
//	genome->code[testTop + 1] = jumpOp(opJumpIfMoreThanZero, reg0, testBottom);
//	genome->code[testTop + 2] = moveBodyOp(2);
//	genome->code[testTop + 3] = jumpOp(opJump, 0, testEnd);
//
//	genome->code[testBottom] = arithmeticOp(opSubtractM, reg0, quorum4, quorum3);
//	genome->code[testBottom + 1] = jumpOp(opJumpIfMoreThanZero, reg0, testElse);
//	genome->code[testBottom + 2] = moveBodyOp(3);
//	genome->code[testBottom + 3] = jumpOp(opJump, 0, testEnd);
//
//	genome->code[testElse ] = moveBodyOpM(0);
//	genome->code[testElse + 1] = jumpOp(opJump, 0, testEnd);
//
//	genome->code[testEnd] = singleOp(opWait);
//	genome->code[testEnd + 1] = jumpOp(opJump, 0, testLeft);
}

vec2f mapToWorldSpace(int32 x, int32 y)
{
	vec2f result = {
			((real32) x - MAP_CENTER_X) * MAP_CELL_SIZE,
			((real32) y - MAP_CENTER_Y) * MAP_CELL_SIZE };
	return result;
}

struct map_coords
{
	int32 x, y;
};

map_coords worldToMapSpace(real32 x, real32 y)
{
	map_coords result = { };
	result.x = (int32) floor(((x) / MAP_CELL_SIZE) + MAP_CENTER_X);
	result.y = (int32) floor(((y) / MAP_CELL_SIZE) + MAP_CENTER_Y);
	return result;
}

bool32 isCoordValid(int32 x, int32 y)
{
	return (x >= 0 && x < MAP_WIDTH && y >= 0 && y < MAP_HEIGHT);
}

uint8 getQuorumValue(quorum_cell* map, int32 x, int32 y)
{
	uint8 result = 0;
	if (isCoordValid(x, y))
	{
		int32 id = x + y * MAP_WIDTH;
		result = map[id].quorum;
	}
	return result;
}

void generateMap(quorum_cell* quorumMap, random_state* random)
{
	for (int32 quorumCellIndex = 0; quorumCellIndex < MAP_WIDTH * MAP_HEIGHT; quorumCellIndex++)
	{
		quorumMap[quorumCellIndex].quorum = 0;
		quorumMap[quorumCellIndex].hasPellet = false;
		if (random->getUint32() % startPelletChance == 0)
		{
			quorumMap[quorumCellIndex].hasPellet = true;
			quorumMap[quorumCellIndex].quorum = 255;
		}
	}
}
void startNewGeneration(game_state* gameState)
{
	gameState->aliveCellCount = MAX_CELLS;
	gameState->genCount++;
	gameState->frameCount = 0;
	random_state* random = &gameState->random;

	const int32 maxSurvivingCells = generationInheritedCells;
	uint64 prevMemSize = gameState->gameMemory.used;

	cell_genome *livingGenome = pushStructArray(&gameState->gameMemory, cell_genome, maxSurvivingCells);
	vec3f *livingColors = pushStructArray(&gameState->gameMemory, vec3f, maxSurvivingCells);
	map_coords* livingCoords = pushStructArray(&gameState->gameMemory, map_coords, maxSurvivingCells);

	gameState->gameMemory.used += sizeof(cell_genome) * maxSurvivingCells;
	Assert(gameState->gameMemory.used < gameState->gameMemory.memorySize)
	int32 highestResourceCells[maxSurvivingCells];
	int32 usedCellCount = 0;
	bool32 foundCell = false;
	do
	{
		foundCell = false;
		uint32 highest = 0;
		for (int32 cellIndex = 0; cellIndex < MAX_CELLS; cellIndex++)
		{
			//Note: we no longer count only the live cells
			if (gameState->cell[cellIndex].aliveSteps > highest || !foundCell)
			{
				highest = gameState->cell[cellIndex].aliveSteps;
				highestResourceCells[usedCellCount] = cellIndex;
				foundCell = true;
			}
		}
		if (foundCell)
		{
			gameState->cell[highestResourceCells[usedCellCount]].aliveSteps = 0;
			usedCellCount++;
		}
	} while (foundCell && usedCellCount < maxSurvivingCells);

	if (usedCellCount == 0)
	{
		usedCellCount = 1;
		generateDefaultGenome(gameState, &gameState->cellCode[0]);
		highestResourceCells[0] = 0;
		gameState->cell[0].color.set(1.0f, 1.0f, 1.0f);
		gameState->cell[0].memory.posX = MAP_CENTER_X;
		gameState->cell[0].memory.posY = MAP_CENTER_Y;
	}

	for (int32 cellIndex = 0; cellIndex < usedCellCount; cellIndex++)
	{
		livingGenome[cellIndex] = gameState->cellCode[highestResourceCells[cellIndex]];
		livingColors[cellIndex] = gameState->cell[highestResourceCells[cellIndex]].color;
		livingCoords[cellIndex].x = gameState->cell[highestResourceCells[cellIndex]].memory.posX;
		livingCoords[cellIndex].y = gameState->cell[highestResourceCells[cellIndex]].memory.posY;
		livingColors[cellIndex].x = clamp(0, 1, livingColors[cellIndex].x + (random->getInt32(-30, 31) / 255.0f));
		livingColors[cellIndex].y = clamp(0, 1, livingColors[cellIndex].y + (random->getInt32(-30, 31) / 255.0f));
		livingColors[cellIndex].z = clamp(0, 1, livingColors[cellIndex].z + (random->getInt32(-30, 31) / 255.0f));
	}

	for (int32 cellIndex = 0; cellIndex < usedCellCount; cellIndex++)
	{
		gameState->cell[cellIndex].isAlive = true;
		gameState->cell[cellIndex].aliveSteps = 0;
		gameState->cell[cellIndex].memory.resources = cellStartingResources;
		gameState->cell[cellIndex].memory.posX = livingCoords[cellIndex].x;
		gameState->cell[cellIndex].memory.posY = livingCoords[cellIndex].y;

		gameState->cell[cellIndex].color = livingColors[cellIndex];
		if (cellIndex < generationUnmodifiedGenomes)
		{
			gameState->cellCode[cellIndex] = livingGenome[cellIndex];			//this guarantees one pure copy of each working cell if one existed
		}
	}

	int32 parentCellIndex = 0;
	int32 usedCells = 0;
	int32 activeGenePoolSize = usedCellCount;

	for (int32 cellIndex = mini(usedCellCount, generationUnmodifiedGenomes); cellIndex < MAX_CELLS; cellIndex++)
	{
		if (usedCells >= activeGenePoolSize)
		{
			usedCells = 0;
			activeGenePoolSize -= activeGenePoolSize / 4;
			if (activeGenePoolSize <= 1)
			{
				activeGenePoolSize = 1;
			}
		}
		parentCellIndex = usedCells % activeGenePoolSize;
		usedCells++;

		ZERO_STRUCT(cell_state, &gameState->cell[cellIndex]);

		gameState->cell[cellIndex].isAlive = true;
		gameState->cell[cellIndex].aliveSteps = 0;
		gameState->cell[cellIndex].memory.resources = cellStartingResources;
		gameState->cell[cellIndex].memory.posX = livingCoords[parentCellIndex].x + random->getInt32(-2, 3);
		gameState->cell[cellIndex].memory.posY = livingCoords[parentCellIndex].y + random->getInt32(-2, 3);
		gameState->cell[cellIndex].color = livingColors[parentCellIndex];

		//We vary the colors so it's easier to see the species further down the line

		int32 opSrcIndex = 0;
		for (int32 opIndex = 0; opIndex < MAX_CODE_LENGTH; opIndex++)
		{

			uint32 mutationType = random->getUint32() % copyErrorChance;
			switch (mutationType)
			{
				case 0: //variation of existing argument
				{
					int variedByte = random->getUint32() % 8;
					gameState->cellCode[cellIndex].code[opIndex].arg8[variedByte] += (int8) random->getInt32(-32, 32);
				}
				break;
				case 1: //new operation
				{
					gameState->cellCode[cellIndex].code[opIndex].op = (int32) (random->getUint32() % opCount);
				}
				break;
				case 2: //copy any other op
				{
					gameState->cellCode[cellIndex].code[opIndex] = livingGenome[parentCellIndex].code[random->getUint32() % MAX_CODE_LENGTH];
				}
				break;
				case 3: //duplicate and insert op
				{
					gameState->cellCode[cellIndex].code[opIndex] = livingGenome[parentCellIndex].code[opSrcIndex % MAX_CODE_LENGTH];
					opSrcIndex--;
				}
				break;
				case 4: //insert noOp
				{
					gameState->cellCode[cellIndex].code[opIndex] = singleOp(opNoOp);
					opSrcIndex--;
				}
				break;
				case 5: //erase currentOp
				{
					gameState->cellCode[cellIndex].code[opIndex] = livingGenome[parentCellIndex].code[opSrcIndex % MAX_CODE_LENGTH];
					opSrcIndex++;
				}
				break;
				default:
				{
					gameState->cellCode[cellIndex].code[opIndex] = livingGenome[parentCellIndex].code[opSrcIndex % MAX_CODE_LENGTH];
				}
			}
			opSrcIndex++;
		}
	}
	generateDefaultGenome(gameState, &gameState->cellCode[MAX_CELLS - 1]);
	generateMap(gameState->quorumMap, random);
	gameState->gameMemory.used = prevMemSize;
}

void updateMap(game_state* gameState)
{
	for (int32 y = 1; y < MAP_HEIGHT - 1; y++)
	{
		for (int32 x = 1; x < MAP_WIDTH - 1; x++)
		{
			int32 id = x + y * MAP_WIDTH;
			if (gameState->quorumMap[id].hasPellet)
			{
				gameState->quorumMap[id].quorum = 255;
			}
			else
			{
				if (gameState->random.getUint32() % pelletChance == pelletChance / 2)
				{
					gameState->quorumMap[id].hasPellet = true;
					gameState->quorumMap[id].quorum = 255;
				}
				else
				{
					int32 above = id - MAP_WIDTH;
					int32 below = id + MAP_WIDTH;
					int32 left = id - 1;
					int32 right = id + 1;
					int32 quorumAverage = gameState->quorumMap[above].quorum +
						gameState->quorumMap[below].quorum +
						gameState->quorumMap[left].quorum +
						gameState->quorumMap[right].quorum;
					quorumAverage /= 4;
					quorumAverage = clampi(0, 255, quorumAverage);
					quorumAverage--;
					quorumAverage += gameState->quorumMap[id].quorum;
					quorumAverage /= 2;
					quorumAverage = clampi(0, 255, quorumAverage);
					gameState->quorumMap[id].quorum = (uint8) (quorumAverage);
				}
			}
		}
	}
}

void executeCellGenome(game_state* gameState, int32 iterations)
{
	for (int32 cellIndex = 0; cellIndex < MAX_CELLS; cellIndex++)
	{
		cell_state* cell = &gameState->cell[cellIndex];
		if (cell->isAlive)
		{
			for (int opCount = 0; opCount < iterations; opCount++)
			{
				if (cell->memory.currentOp >= 0 && cell->memory.currentOp < MAX_CODE_LENGTH)
				{
					//cell->memory.currentOp is the next op to be executed

					instruction_state curInstruction = gameState->cellCode[cellIndex].code[cell->memory.currentOp];
					switch (curInstruction.op)
					{
						case opMoveBody: // left=0,right,up,down, only first two bits count U8[0]
						{
							//moves body, forces next step
							uint32 dir = curInstruction.arg8[0] & 3;
							cell->movement |= 1 << dir;

						}
						break;
						case opMoveBodyM: // left=0,right,up,down, only first two bits count U8[0]
						{
							//moves body, forces next step
							uint32 dir = (uint32) (cell->getValue(curInstruction.uarg16[0]) & 3);
							cell->movement |= 1 << dir;
						}
						break;
#define VALUE_ARITHMETIC(op) cell->storeValue(curInstruction.uarg16[0], cell->getValue(curInstruction.uarg16[1]) op curInstruction.arg32[1])
#define MEMORY_ARITHMETIC(op) cell->storeValue(curInstruction.uarg16[0], cell->getValue(curInstruction.uarg16[1]) op cell->getValue(curInstruction.uarg16[2]))
						case opAdd: 		//dstAddress U16[0], srcAddress U16[1], srcValue S32[1]
						VALUE_ARITHMETIC(+);
						break;
						case opSubtract: 		//dstAddress U16[0], srcAddress U16[1], srcValue S32[1]
						VALUE_ARITHMETIC(-);
						break;
						case opMultiply: 		//dstAddress U16[0], srcAddress U16[1], srcValue S32[1]
						VALUE_ARITHMETIC(*);
						break;
						case opDivide: 		//dstAddress U16[0], srcAddress U16[1], srcValue S32[1]
						{
							int32 divisor = curInstruction.uarg32[1];
							if (divisor != 0)
							{
								cell->storeValue(curInstruction.uarg16[0], (cell->getValue(curInstruction.uarg16[1]) / divisor));
							}
							else
							{
								cell->storeValue(curInstruction.uarg16[0], 0);
							}
						}
						break;
						case opBitAnd: 	//dstAddress U16[0], srcAddress U16[1], srcValue S32[1]
						VALUE_ARITHMETIC(&);
						break;
						case opBitOr: 	//dstAddress U16[0], srcAddress U16[1], srcValue S32[1]
						VALUE_ARITHMETIC(|);
						break;
						case opBitNot: 	//dstAddress U16[0], srcAddress U16[1]
						{
							cell->storeValue(curInstruction.uarg16[0], cell->getValue(~curInstruction.uarg16[1]));
						}
						break;
						case opAddM: 	//dstAddress U16[0], srcAddressLhs U16[1], srcAddressRhs U16[2]
						MEMORY_ARITHMETIC(+);
						break;
						case opSubtractM: 	//dstAddress U16[0], srcAddressLhs U16[1], srcAddressRhs U16[2]
						MEMORY_ARITHMETIC(-);
						break;
						case opMultiplyM: 	//dstAddress U16[0], srcAddressLhs U16[1], srcAddressRhs U16[2]
						MEMORY_ARITHMETIC(*);
						break;
						case opDivideM: 	//dstAddress U16[0], srcAddressLhs U16[1], srcAddressRhs U16[2]
						{
							int32 divisor = cell->getValue(curInstruction.uarg16[2]);
							if (divisor != 0)
							{
								cell->storeValue(curInstruction.uarg16[0], (cell->getValue(curInstruction.uarg16[1]) / divisor));
							}
							else
							{
								cell->storeValue(curInstruction.uarg16[0], 0);
							}
						}
						break;
						case opBitAndM: 	//dstAddress U16[0], srcAddressLhs U16[1], srcAddressRhs U16[2]
						MEMORY_ARITHMETIC(&);
						break;
						break;
						case opBitOrM: 	//dstAddress U16[0], srcAddressLhs U16[1], srcAddressRhs U16[2]
						MEMORY_ARITHMETIC(|);
						break;

						case opCopy:
						//(dstAddress U16[0],srcAddress)
						{
							cell->storeValue(curInstruction.uarg16[0], cell->getValue(curInstruction.uarg16[1]));
						}
						break;
						case opSet:
						//(dstAddress U16[0],value)
						{
							cell->storeValue(curInstruction.uarg16[0], curInstruction.arg32[1]);
						}
						break;

						case opJumpIfEqualZero:
						// srcAddress, opIndex
						{
							if (cell->getValue(curInstruction.uarg16[0]) == 0)
							{
								cell->memory.currentOp = curInstruction.uarg32[1] - 1;
							}
						}
						break;
						case opJumpIfLessThanZero:
						// srcAddress, opIndex
						{
							if (cell->getValue(curInstruction.uarg16[0]) < 0)
							{
								cell->memory.currentOp = curInstruction.uarg32[1] - 1;
							}
						}
						break;
						case opJumpIfLessOrEqualZero:
						// srcAddress, opIndex
						{
							if (cell->getValue(curInstruction.uarg16[0]) <= 0)
							{
								cell->memory.currentOp = curInstruction.uarg32[1] - 1;
							}
						}
						break;
						case opJumpIfMoreThanZero:
						// srcAddress, opIndex
						{
							if (cell->getValue(curInstruction.uarg16[0]) > 0)
							{
								cell->memory.currentOp = curInstruction.uarg32[1] - 1;
							}
						}
						break;
						case opJumpIfMoreOrEqual:
						// srcAddress, opIndex
						{
							if (cell->getValue(curInstruction.uarg16[0]) >= 0)
							{
								cell->memory.currentOp = curInstruction.uarg32[1] - 1;
							}
						}
						break;
						case opJump:
						//opIndex
						{
							cell->memory.currentOp = curInstruction.uarg32[1] - 1;
						}
						break;

						case opWait:
						{
							opCount = 16;
						}
						break;
					}
					cell->memory.currentOp++;
				}
			}
		}
	}
}

void updateCells(game_state* gameState)
{
	int32 aliveCells = gameState->aliveCellCount;

	for (int32 cellIndex = 0; cellIndex < MAX_CELLS; cellIndex++)
	{
		cell_state* cell = &gameState->cell[cellIndex];
		if (cell->isAlive)
		{
			if (cell->movement & 1)
			{
				cell->memory.posX--;
			}
			if (cell->movement & 2)
			{
				cell->memory.posX++;
			}
			if (cell->movement & 4)
			{
				cell->memory.posY--;
			}
			if (cell->movement & 8)
			{
				cell->memory.posY++;
			}
			cell->movement = 0;
			cell->memory.resources -= resourcePerStepConsumption;
			if (cell->memory.resources <= 0)
			{
				cell->memory.resources = 0;
				cell->isAlive = false;
				aliveCells--;
			}

			if (isCoordValid(cell->memory.posX, cell->memory.posY))
			{
				int32 id = cell->memory.posX + cell->memory.posY * MAP_WIDTH;
				if (gameState->quorumMap[id].hasPellet)
				{
					gameState->quorumMap[id].hasPellet = false;
					gameState->quorumMap[id].quorum = 0;
					cell->memory.resources += pelletResourceGain;
				}
			}
			cell->aliveSteps++;

			cell->memory.quorum[0] = getQuorumValue(gameState->quorumMap, cell->memory.posX - 1, cell->memory.posY);
			cell->memory.quorum[1] = getQuorumValue(gameState->quorumMap, cell->memory.posX + 1, cell->memory.posY);
			cell->memory.quorum[2] = getQuorumValue(gameState->quorumMap, cell->memory.posX, cell->memory.posY - 1);
			cell->memory.quorum[3] = getQuorumValue(gameState->quorumMap, cell->memory.posX, cell->memory.posY + 1);
			cell->memory.quorum[4] = getQuorumValue(gameState->quorumMap, cell->memory.posX, cell->memory.posY);
		}
	}
	gameState->aliveCellCount = aliveCells;
}

GAME_UPDATE_AND_RENDER(GAME_UPDATE_AND_RENDER_NAME)
{
	game_state *gameState = (game_state*) (memory->gameMemory);
	game_renderer *renderer = memory->renderer;
	game_renderer *debugRenderer = memory->debugRenderer;
	font_manager *fontManager = memory->fontManager;

	if (!memory->isInitialized)
	{
		memory->isInitialized = true;

		gameState->gameMemory.memory = memory->gameMemory + sizeof(game_state);
		gameState->gameMemory.memorySize = memory->gameMemorySize - sizeof(game_state);
		gameState->gameMemory.used = 0;

		gameState->debugFont = fontManager->getFont(Font::TestFont);

		gameState->areGraphicsEnabled = true;

		gameState->isHelpVisible = true;
		gameState->isInfoVisible = false;
		gameState->areGraphicsEnabled = true;
		gameState->isInDebugMode = false;

		gameState->stepsPerFrame = 1;

		gameState->uiCamera.anchor.set(0.0f, 0.0f);
		gameState->uiCamera.zoom = 1.0f;
		gameState->uiCamera.viewport.x1 = 0;
		gameState->uiCamera.viewport.x2 = (real32) screen->width;
		gameState->uiCamera.viewport.y1 = 0;
		gameState->uiCamera.viewport.y2 = (real32) screen->height;

		//TODO: move the camera position and not the map position
		gameState->worldCamera.anchor.set(0.5f, 0.5f);
		gameState->worldCamera.zoom = 1.0f;
		gameState->worldCamera.viewport.x1 = 0;
		gameState->worldCamera.viewport.x2 = (real32) screen->width;
		gameState->worldCamera.viewport.y1 = 0;
		gameState->worldCamera.viewport.y2 = (real32) screen->height;

		gameState->cellSprite = renderer->getImage(Image::Cell);
		gameState->foodPelletSprite = renderer->getImage(Image::FoodPellet);

		gameState->cell[0].isAlive = true;
		gameState->cell[0].memory.resources = cellStartingResources;
		gameState->cell[0].memory.posX = MAP_WIDTH / 2;
		gameState->cell[0].memory.posY = MAP_HEIGHT / 2;
		gameState->cell[0].color.set(1, 1, 1);

		gameState->selectedCell = 0;

		generateDefaultGenome(gameState, &gameState->cellCode[0]);
		gameState->aliveCellCount = MAX_CELLS;
		random_state* random = &gameState->random;
		for (int32 cellIndex = 1; cellIndex < MAX_CELLS; cellIndex++)
		{
			gameState->cell[cellIndex] = gameState->cell[0];
			gameState->cellCode[cellIndex] = gameState->cellCode[0];
		}

		generateMap(gameState->quorumMap, random);
	}

	bool32 prevGraphicsEnabled = gameState->areGraphicsEnabled;

	//TODO: zoom according to mouse position, allow for dragging camera around
	if (input->buttonZoomIn.wasPressed())
	{
		gameState->worldCamera.zoom *= 0.9f;
		if (gameState->worldCamera.zoom < 0.01f)
		{
			gameState->worldCamera.zoom = 0.01f;
		}
	}
	if (input->buttonZoomOut.wasPressed())
	{
		gameState->worldCamera.zoom *= 1.1f;
	}

	real32 worldLeft = (real32) -screen->width * 0.5f;
	real32 worldTop = (real32) -screen->height * 0.5f;
	real32 worldRight = worldLeft + screen->width;
	real32 worldBottom = worldTop + screen->height;

	gameState->worldCamera.startCamera(debugRenderer);

	if (input->buttonToggleHelp.wasPressed())
	{
		gameState->isHelpVisible = !gameState->isHelpVisible;
	}

	if (input->buttonToggleInfo.wasPressed())
	{
		gameState->isInfoVisible = !gameState->isInfoVisible;
	}
	if (input->buttonToggleGraphics.wasPressed())
	{
		gameState->areGraphicsEnabled = !gameState->areGraphicsEnabled;
	}

	if (input->buttonToggleDebug.wasPressed())
	{
		gameState->isInDebugMode = !gameState->isInDebugMode;
		gameState->debugExecutedCodeSteps = 0;
		gameState->stepsPerFrame = 1;
	}

	if (input->buttonSelectNext.wasPressed())
	{
		for (int32 cellIndex = (gameState->selectedCell + 1) % MAX_CELLS; cellIndex != gameState->selectedCell; cellIndex = (cellIndex + 1) % MAX_CELLS)
		{
			if (gameState->cell[cellIndex].isAlive)
			{
				gameState->selectedCell = cellIndex;
				break;
			}
		}
	}

	if (gameState->isInDebugMode)
	{
		if (input->buttonNextOp.wasPressed())
		{
			//execute only one operation, and then continue, only actually perform the step after a fixed number of operations
			//Note: using debug mode means the simulation is no longer deterministic because I was too lazy to ensure it still was
			executeCellGenome(gameState, 1);
			gameState->debugExecutedCodeSteps++;
			if (gameState->debugExecutedCodeSteps == opCountPerStep)
			{
				updateCells(gameState);
				gameState->frameCount++;
				if ((gameState->frameCount % generationMaxSimulationSteps) == 0 || (uint32) gameState->aliveCellCount < generationMinLivingCells)
				{
					startNewGeneration(gameState);
				}
				updateMap(gameState);
			}
		}
	}
	else
	{
		if (input->buttonSpeedUp.isDown)
		{
			gameState->stepsPerFrame++;
		}
		else if (input->buttonSlowDown.isDown)
		{
			gameState->stepsPerFrame--;
			if (gameState->stepsPerFrame <= 0)
			{
				gameState->stepsPerFrame = 1;
			}
		}

		for (int32 stepIndex = 0; stepIndex < gameState->stepsPerFrame; stepIndex++)
		{
			gameState->frameCount++;

			if ((gameState->frameCount % generationMaxSimulationSteps) == 0 || (uint32) gameState->aliveCellCount < generationMinLivingCells)
			{
				if (input->buttonStopOnGeneration.isDown)
				{
					gameState->stepsPerFrame = 1;
				}
				startNewGeneration(gameState);
			}

			updateMap(gameState);
			executeCellGenome(gameState, opCountPerStep);
			updateCells(gameState);
		}
	}

	if (input->buttonMouseLeft.wasPressed())
	{
		vec2f mousePos = (
			_vec2f(input->mouseX, input->mouseY)
				- _vec2f(screen->width * 0.5f, screen->height * 0.5f))
			/ gameState->worldCamera.zoom
			+ gameState->worldCamera.pos;
		map_coords mouseCoords = worldToMapSpace(mousePos.x, mousePos.y);
		printf("checking for cell at %d, %d\n", mouseCoords.x, mouseCoords.y);
		for (int32 cellIndex = 0; cellIndex < MAX_CELLS; cellIndex++)
		{
			cell_state* cell = &gameState->cell[cellIndex];
			if (cell->isAlive && cell->memory.posX == mouseCoords.x && cell->memory.posY == mouseCoords.y)
			{
				gameState->selectedCell = cellIndex;
				break;
			}
		}
	}

	if (input->buttonMouseRight.wasPressed() && input->buttonMouseRight.isDown)
	{
		gameState->prevMousePos.x = input->mouseX;
		gameState->prevMousePos.y = input->mouseY;
	}
	else if (input->buttonMouseRight.isDown)
	{
		gameState->worldCamera.pos.x += (gameState->prevMousePos.x - input->mouseX) / gameState->worldCamera.zoom;
		gameState->worldCamera.pos.y += (gameState->prevMousePos.y - input->mouseY) / gameState->worldCamera.zoom;

		gameState->prevMousePos.x = input->mouseX;
		gameState->prevMousePos.y = input->mouseY;
	}

	if (gameState->areGraphicsEnabled)
	{
		gameState->uiCamera.viewport.set(0, 0, (real32) screen->width, (real32) screen->height);
		gameState->worldCamera.viewport.set(0, 0, (real32) screen->width, (real32) screen->height);

		gameState->worldCamera.startCamera(renderer);
		renderer->clearView(0.0f, 0.0f, 0.0f);

		for (int32 y = 0; y < MAP_HEIGHT; y++)
		{
			for (int32 x = 0; x < MAP_WIDTH; x++)
			{
				vec2f pos = {
						((real32) x - MAP_CENTER_X) * MAP_CELL_SIZE,
						((real32) y - MAP_CENTER_Y) * MAP_CELL_SIZE };
				int32 id = x + y * MAP_WIDTH;

				real32 quorumRatio = gameState->quorumMap[id].quorum / 255.0f;
				renderer->drawRectangle(pos.x, pos.y, pos.x + MAP_CELL_SIZE, pos.y + MAP_CELL_SIZE, false, quorumRatio, 0, 0);

				if (gameState->quorumMap[id].hasPellet)
				{
					renderer->drawImage(pos.x, pos.y, pos.x + MAP_CELL_SIZE, pos.y + MAP_CELL_SIZE, gameState->foodPelletSprite, 1.0f, 1.0f, 0);
				}
			}
		}

		for (int32 cellIndex = 0; cellIndex < MAX_CELLS; cellIndex++)
		{
			cell_state* cell = &gameState->cell[cellIndex];
			if (cell->isAlive)
			{
				vec2f cellPos = mapToWorldSpace(cell->memory.posX, cell->memory.posY);
				renderer->drawImage(
					cellPos.x, cellPos.y,
					cellPos.x + MAP_CELL_SIZE, cellPos.y + MAP_CELL_SIZE,
					gameState->cellSprite,
					cell->color.x,
					cell->color.y,
					cell->color.z
					);

//				renderer->drawCircle(
//					cellPos.x + MAP_CELL_SIZE / 2,
//					cellPos.y + MAP_CELL_SIZE / 2,
//					MAP_CELL_SIZE / 2,
//					16, true,
//					0.4f, 0.4f, 1
//					);
			}
		}

		cell_state* selectedCell = &gameState->cell[gameState->selectedCell];
		if (selectedCell->isAlive)
		{
			vec2f cellPos = mapToWorldSpace(selectedCell->memory.posX, selectedCell->memory.posY);
			renderer->drawCircle(
				cellPos.x + MAP_CELL_SIZE / 2,
				cellPos.y + MAP_CELL_SIZE / 2,
				MAP_CELL_SIZE / 2 + 5,
				16, true,
				0, 1, 1
				);

			renderer->drawImage(
				cellPos.x - 2, cellPos.y - 2,
				cellPos.x + MAP_CELL_SIZE + 2, cellPos.y + MAP_CELL_SIZE + 2,
				gameState->cellSprite,
				selectedCell->color.x,
				selectedCell->color.y,
				selectedCell->color.z
				);
		}
	}
	else
	{
		gameState->uiCamera.startCamera(renderer);
		char buff[1024 * 4];

		real32 textBackgroundAlpha = 0.9f;
		sprintf_s(buff, "GRAPHICS UPDATE DISABLED\n"
			"   press F3 to enable   ");
		font_render_mesh_state graphicsDisabledTextMesh = fontManager->createTemporaryText(buff,
			0, 0,
			64.0f,
			gameState->debugFont,
			1.0f, 1.0f, 1.0f, 1.0f);
		renderer->drawRectangle(
			0, 0,
			(real32) screen->width, (real32) screen->height,
			false, 0.0f, 0.0f, 0.0f, textBackgroundAlpha);
		renderer->pushMatrix();
		renderer->mulTransform(
			(screen->width - graphicsDisabledTextMesh.boundingBox.getWidth()) / 2.0f,
			(screen->height - graphicsDisabledTextMesh.boundingBox.getHeight()) / 2.0f,
			1.0f, 1.0f, 0.0f
			);
		renderer->drawMesh(graphicsDisabledTextMesh.mesh);
		renderer->popMatrix();
	}

	gameState->uiCamera.startCamera(renderer);

	cell_state* curCell = &gameState->cell[gameState->selectedCell];
	instruction_state curInstruction = { };
	int32 curOp = curCell->memory.currentOp;
	if (curOp < MAX_CODE_LENGTH && curOp >= 0)
	{
		curInstruction = gameState->cellCode[gameState->selectedCell].code[curCell->memory.currentOp];
	}

	real32 textXStart = 40;
	real32 textYStart = 40;
	char buff[1024 * 4];

	real32 textBackgroundAlpha = 0.9f;

	if (gameState->isInDebugMode)
	{
		sprintf_s(buff, "DEBUG MODE ENABLED");
		font_render_mesh_state debugTextMesh = fontManager->createTemporaryText(buff,
			textXStart, textYStart,
			64.0f,
			gameState->debugFont,
			1.0f, 0.0f, 0.0f, 1.0f);
		renderer->drawRectangle(
			debugTextMesh.boundingBox.minX, debugTextMesh.boundingBox.minY,
			debugTextMesh.boundingBox.maxX, debugTextMesh.boundingBox.maxY,
			false, 0.0f, 0.0f, 0.0f, textBackgroundAlpha);
		renderer->drawMesh(debugTextMesh.mesh);
		textYStart += debugTextMesh.boundingBox.getHeight();
	}

	if (gameState->isHelpVisible)
	{
		sprintf_s(buff,
			"Toggle Help: --------- F1\n"
				"Toggle Info: --------- F2\n"
				"Toggle Drawing: ------ F3\n"
				"Toggle Debug Mode: --- Space\n"
				"Zoom in/out: --------- Mouse Wheel\n"
				"Speed up/down(hold): - Keypad +/-\n"
				"Slow Down on next gen: Enter\n"
				"Select next/prev cell: Right/Left Arrows\n"
				"Select cell: --------- Left Mouse\n"
				"(debug)Next op: ------ Down Arrow"
			);
		font_render_mesh_state helpTextMesh = fontManager->createTemporaryText(buff,
			textXStart, textYStart,
			32.0f,
			gameState->debugFont,
			1.0f, 1.0f, 1.0f, 1.0f);
		renderer->drawRectangle(
			helpTextMesh.boundingBox.minX, helpTextMesh.boundingBox.minY,
			helpTextMesh.boundingBox.maxX, helpTextMesh.boundingBox.maxY,
			false, 0.0f, 0.0f, 0.0f, textBackgroundAlpha);
		renderer->drawMesh(helpTextMesh.mesh);
		textYStart += helpTextMesh.boundingBox.getHeight();
	}

	if (gameState->isInfoVisible)
	{
		sprintf_s(buff,
			"Steps per frame: -----  %d\n"
				"Generation: ----------- %d\n"
				"Living Cells: --------- %d\n"
				"selected cell info: --- %d\n"
				"resources: ------------ %d\n"
				"pos: ------------------ (%d,%d)\n"
				"currentOp: ------------ %d\n"
				"currentInstruction: --- %s(%d,%d)"
			,
			gameState->stepsPerFrame,
			gameState->genCount,
			gameState->aliveCellCount,
			gameState->selectedCell,
			gameState->cell[gameState->selectedCell].memory.resources,
			gameState->cell[gameState->selectedCell].memory.posX,
			gameState->cell[gameState->selectedCell].memory.posY,
			gameState->cell[gameState->selectedCell].memory.currentOp,
			op_names[curInstruction.op],
			curInstruction.arg32[0],
			curInstruction.arg32[1]
			);

		font_render_mesh_state infoTextMesh = fontManager->createTemporaryText(
			buff,
			textXStart, textYStart,
			32.0f,
			gameState->debugFont,
			1.0f, 1.0f, 1.0f, 1.0f);
		renderer->drawRectangle(
			infoTextMesh.boundingBox.minX, infoTextMesh.boundingBox.minY,
			infoTextMesh.boundingBox.maxX, infoTextMesh.boundingBox.maxY,
			false, 0, 0, 0, textBackgroundAlpha);
		renderer->drawMesh(infoTextMesh.mesh);
	}

}

#endif

